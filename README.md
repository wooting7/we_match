# 사원관리 APP
***
소개: 지역별 운동 매칭하기 위한 앱 입니다
***
#### * 목차 *
```
1. 언어
2. 기능
3. APP 이미지
```
#### 1. 언어
```
DART
FLUTTER
```
#### 2. 기능
```
* 회원관리
  - (사용자) 회원 가입
  - (사용자) 회원 비밀번호 변경
  - (사용자) 프로필 변경
  - (사용자) 회원 탈퇴
    
* 로그인
  - (사용자) 앱 - 일반유저 로그인
  
* 매치관리
  - (사용자) (post) 나의매치 등록
  - (사용자) (get)  나의매치 리스트 조회
  - (사용자) (get)  매치 리스트 상세 조회
  - (사용자) (put)  나의매치 수정
  - (사용자) (put)  나의매치 삭제
  - (사용자) (post) 매치 참가신청 등록
  - (사용자) (get)  매치에 신청한 인원 리스트 조회(승락, 거부, 미처리)
  - (사용자) (get)  신청한 매치 리스트 조회(참가, 거절, 대기)
  - (사용자) (put)  매치 신청처리(승락, 거부, 미처리)
  - (사용자) (get)  동일지역 매치 리스트 조회
  
* 문의관리
  - (사용자) (post) 문의 등록
  - (사용자) (get)  내 문의 조회
  - (공용자) (get)  문의답글 리스트 조회 
  - (사용자) (get)  문의사항 세부정보 조회
  
```

#### 3. APP 이미지

>* 로그인 & 회원가입
>
>![app_login](./assets/app_login.png)

>* 매치 & 매치상세
>
>![app_match](./assets/app_match.png)

>* 나의매치 & 나의매치상세
>
>![app_my_match](./assets/app_my_match.png)

>* 참가내역 & 참가내역상세
>
>![app_join_match](./assets/app_join_match.png)

>* 나의정보 & 설정
>
>![app_info](./assets/app_info.png)

>* 로그아웃 & 로그아웃 성공
>
>![app_logout](./assets/app_logout.png)

>* 프로필변경 & 문의글쓰기
>
>![app_member_update](./assets/app_member_update.png)

>* 나의문의 & 나의문의상세
>
>![app_complain](./assets/app_complain.png)
