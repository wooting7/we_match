import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../component/component_notification.dart';
import '../page/page_login.dart';

/*
SharedPreferences 에 데이터가 저장되는 방식은 Map임을 반드시 숙지하고 갈 것.
Map이 무엇인지 알아야 함.
 */
class TokenLib {
  static Future<String?> getToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString('token');
  }

  static void setToken(String token) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('token', token);
  }

  static void logout(BuildContext context) async {
    BotToast.closeAllLoading();

    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('token', '');

    ComponentNotification(
        success: false,
        title: '로그아웃',
        subTitle: '로그아웃되어 로그인 화면으로 이동합니다.'
    ).call();

    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext cotext) => const PageLogin()), (route) => false);
  }
}
