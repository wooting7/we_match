import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:we_match/model/complain/complain_request.dart';
import 'package:we_match/repository/repo_complain.dart';

import '../component/component_appbar_actions_back.dart';
import '../component/component_custom_loading.dart';
import '../component/component_notification.dart';
import '../component/component_outline_button.dart';
import '../config/config_color.dart';
import '../config/config_decoration.dart';
import '../config/config_form_validator.dart';

class PageComplainNew extends StatefulWidget {
  const PageComplainNew({Key? key}) : super(key: key);

  @override
  State<PageComplainNew> createState() => _PageComplainNewState();
}

class _PageComplainNewState extends State<PageComplainNew> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setComplain(ComplainRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoComplain().setComplain(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: true,
        title: '문의 등록 성공',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: false,
        title: '문의 등록 실패',
        subTitle: '문의 등록에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActionsBack(
        title: '문의 글쓰기',
      ),
      body: _BuildBody(),
      backgroundColor: colorSecondary,
    );
  }

  Widget _BuildBody() {
    return SingleChildScrollView(
        child: Container(
          padding: bodyPaddingAll,
          margin: bodyPaddingAll,
          decoration: BoxDecoration(
            color: colorWhite,
            borderRadius: BorderRadius.circular(10),
          ),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  FormBuilderTextField(
                    name: 'title',
                    decoration: const InputDecoration(
                        labelText: '매칭제목', hintText: '2~30자 입력해주세요'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2,
                          errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(30,
                          errorText: formErrorMaxLength(30)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  FormBuilderTextField(
                    maxLength: 300,
                    maxLines: 10,
                    name: 'contents',
                    decoration: const InputDecoration(
                        labelText: '글 내용', hintText: '2~300자 입력해주세요'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2,
                          errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(300,
                          errorText: formErrorMaxLength(300)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ComponentOutlineButton(
                      callback: () {
                        if (_formKey.currentState?.saveAndValidate() ?? false) {
                          ComplainRequest request = ComplainRequest(
                            _formKey.currentState!.fields['title']!.value,
                            _formKey.currentState!.fields['contents']!.value,
                          );
                          _setComplain(request);
                        }
                      },
                      name: '완료')
                ]),
          ),
        ));
  }
}