import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:we_match/component/component_appbar_actions_back.dart';
import 'package:we_match/component/component_complain_answer_list_item.dart';
import 'package:we_match/component/component_custom_loading.dart';
import 'package:we_match/component/component_list_item_row.dart';
import 'package:we_match/component/component_notification.dart';
import 'package:we_match/config/config_color.dart';
import 'package:we_match/config/config_decoration.dart';
import 'package:we_match/model/complain/complain_answer_item.dart';
import 'package:we_match/model/complain/complain_detail.dart';
import 'package:we_match/repository/repo_complain.dart';
import 'package:we_match/repository/repo_complain_answer.dart';

class PageComplainDetail extends StatefulWidget {
  const PageComplainDetail({super.key, required this.complainId});

  final int complainId;

  @override
  State<PageComplainDetail> createState() => _PageComplainDetailState();
}

class _PageComplainDetailState extends State<PageComplainDetail> {
  List<ComplainAnswerItem> _list = [];

  ComplainDetail _detail = ComplainDetail(0, '', '', '', '');

  @override
  void initState() {
    super.initState();
    _complainDetail();
    _getComplainAnswerList();
  }

  Future<void> _complainDetail() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoComplain().getMatchDetail(widget.complainId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _detail = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      Navigator.pop(context);
    });
  }

  Future<void> _getComplainAnswerList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoComplainAnswer().getComplainAnswerList(widget.complainId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorSecondary,
      appBar: ComponentAppbarActionsBack(
        title: '나의문의상세',
      ),
      body: SingleChildScrollView(
        child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                transformAlignment: Alignment.topLeft,
                padding: bodyPaddingAll,
                margin: bodyPaddingAll,
                decoration: BoxDecoration(
                  color: colorWhite,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('${_detail.title}',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Container(
                        child: ComponentListITemRow(
                            name: '등록일: ${_detail.dateCreate}',
                            variableName: '처리상태: ${_detail.processStateName}'),
                      ),
                      Text('${_detail.contents}'),
                      SizedBox(height: 10,),
                      Divider(),
                      SizedBox(height: 10,),
                      _buildList(),
                    ]),
              ),
            ]),
      ),
    );
  }


  Widget _buildList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentComplainAnswerListItem(
              item: _list[index],
            ),
          ),
        ],
      ),
    );
  }
}
