import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:we_match/page/page_login.dart';

import '../component/component_appbar_actions_back.dart';
import '../component/component_custom_loading.dart';
import '../component/component_notification.dart';
import '../component/component_outline_button_login.dart';
import '../config/config_color.dart';
import '../config/config_decoration.dart';
import '../functions/token_lib.dart';
import '../repository/repo_member.dart';

class PageSetting extends StatefulWidget {
  const PageSetting({Key? key}) : super(key: key);

  @override
  State<PageSetting> createState() => _PageSettingState();
}

class _PageSettingState extends State<PageSetting> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActionsBack(
        title: '설정',
      ),
      body: _BuildBody(),
      backgroundColor: colorSecondary,
    );
  }

  void _showLogoutDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("로그아웃"),
            content: Container(child: const Text('정말로 로그아웃하시겠습니까?')),
            actions: [
              CupertinoButton(
                onPressed: () {
                  TokenLib.logout(context);
                },
                child: const Text("로그아웃"),
              ),
              CupertinoButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text("닫기"),
              ),
            ],
          );
        });
  }

  Future<void> _putWithdraw() async {
    // 여기서부터
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    // 여기까지 커스텀로딩 띄우는 소스

    await RepoMember().putWithdraw().then((res) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: true,
        title: '회원 탈퇴 성공',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();

      // 창 닫으면서 다 처리하고 정상적으로 닫혔다!! 라고 알려주기
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext cotext) => const PageLogin()), (route) => false // <- 이부분.. 다 처리하고 정상적으로 닫혔다!!

      );
    }).catchError((err) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: false,
        title: '회원 탈퇴 실패',
        subTitle: '회원 탈퇴에 실패하였습니다.',
      ).call();
    });
  }

  void _showWithdrawDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("회원탈퇴"),
            content: Container(child: const Text('정말로 탈퇴하시겠습니까?')),
            actions: [
              CupertinoButton(
                onPressed: () {
                  _putWithdraw();
                },
                child: const Text("회원탈퇴"),
              ),
              CupertinoButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text("닫기"),
              ),
            ],
          );
        });
  }

  Widget _BuildBody() {
    return Scaffold(
      body: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
                margin: EdgeInsets.all(20.0),
                padding: bodyPaddingAll,
                decoration: BoxDecoration(
                  color: colorPrimary,
                ),
                child:
                    ComponentOutlineButtonLogin(callback: () {_showLogoutDialog();}, name: '로그아웃')),
            const SizedBox(
              height: 20,
            ),
            Container(
                margin: EdgeInsets.all(20.0),
                padding: bodyPaddingAll,
                decoration: BoxDecoration(
                  color: colorPrimary,
                ),
                child:
                    ComponentOutlineButtonLogin(callback: () {_putWithdraw();}, name: '회원탈퇴')),
          ]),
      backgroundColor: colorSecondary,
    );
  }
}
