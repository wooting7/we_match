import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:we_match/component/component_appbar_actions_back.dart';
import 'package:we_match/component/component_custom_loading.dart';
import 'package:we_match/component/component_list_item_row.dart';
import 'package:we_match/component/component_notification.dart';
import 'package:we_match/component/component_outline_button.dart';
import 'package:we_match/config/config_color.dart';
import 'package:we_match/config/config_decoration.dart';
import 'package:we_match/model/match/match_detail.dart';
import 'package:we_match/repository/repo_match.dart';
import 'package:we_match/repository/repo_match_apply.dart';

class PageHomeDetail extends StatefulWidget {
  const PageHomeDetail({super.key, required this.matchId});

  final int matchId;

  @override
  State<PageHomeDetail> createState() => _PageHomeDetailState();
}

class _PageHomeDetailState extends State<PageHomeDetail> {
  MatchDetail _detail = MatchDetail(0, '', '', '', '', '', '', 0, '', '');

  @override
  void initState() {
    super.initState();
    _matchDetail();
  }

  Future<void> _setMatchApply(int matchId) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMatchApply().setMatchApply(matchId).then((res) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: true,
        title: '매치 신청 성공',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: false,
        title: '매치 신청 실패',
        subTitle: '매치 신청에 실패하였습니다.',
      ).call();
    });
  }

  Future<void> _matchDetail() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMatch().getMatchDetail(widget.matchId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _detail = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorSecondary,
      appBar: ComponentAppbarActionsBack(
        title: '매치상세',
      ),
      body: SingleChildScrollView(
        child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ClipRRect(
                child: Image.asset(
                  'assets/match_img.png',
                  height: 300,
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                transformAlignment: Alignment.topLeft,
                padding: bodyPaddingAll,
                margin: bodyPaddingAll,
                decoration: BoxDecoration(
                  color: colorWhite,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('${_detail.title}',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Text('${_detail.addressName}'),
                      Container(
                        child: ComponentListITemRow(
                            name: '경기일: ${_detail.sportDate}',
                            variableName: '생성일: ${_detail.dateCreate}'),
                      ),
                      Divider(),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        '${_detail.contents}내용',
                        maxLines: 10,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                              child: ComponentOutlineButton(
                                  callback: () {
                                    _setMatchApply(widget.matchId);
                                  },
                                  name: '신청하기')),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                              child: ComponentOutlineButton(
                                  callback: () {}, name: '취소하기')),
                        ],
                      )
                    ]),
              ),
            ]),
      ),
    );
  }
}
