import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:we_match/component/component_appbar_popup.dart';

class PageHomeSearch extends StatefulWidget {
  PageHomeSearch({super.key, this.title = ''});

  String title;

  @override
  State<PageHomeSearch> createState() => _PageHomeSearchState();
}

class _PageHomeSearchState extends State<PageHomeSearch> {
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '제목 검색',
      ),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 40,
          child: OutlinedButton(
            onPressed: () {
              String inputTitle = _formKey.currentState!.fields['title']!.value;

              Navigator.pop(
                context,
                [true, inputTitle],
              );
            },
            child: const Text('검색하기'),
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                FormBuilderTextField(
                  name: 'title',
                  initialValue: widget.title,
                  decoration: const InputDecoration(labelText: '제목'),
                ),
              ]),
        ),
      ],
    );
  }
}
