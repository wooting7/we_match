import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:we_match/component/component_appbar_actions_back.dart';
import 'package:we_match/component/component_complain_list_item.dart';
import 'package:we_match/component/component_count_title.dart';
import 'package:we_match/component/component_custom_loading.dart';
import 'package:we_match/component/component_outline_button.dart';
import 'package:we_match/config/config_color.dart';
import 'package:we_match/config/config_decoration.dart';
import 'package:we_match/config/config_dropdown.dart';
import 'package:we_match/model/complain/complain_item.dart';
import 'package:we_match/page/page_complain_detail.dart';
import 'package:we_match/repository/repo_complain.dart';

class PageComplainList extends StatefulWidget {
  const PageComplainList({Key? key}) : super(key: key);

  @override
  State<PageComplainList> createState() => _PageComplainListState();
}

class _PageComplainListState extends State<PageComplainList> {
  int _selectedYear = DateTime
      .now()
      .year;
  int _selectedMonth = DateTime
      .now()
      .month;

  List<ComplainItem> _list = [];
  int _totalItemCount = 0;

  Future<void> _loadItems() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoComplain()
        .getMyComplainList(_selectedYear, _selectedMonth)
        .then((res) =>
    {
      BotToast.closeAllLoading(),
      setState(() {
        _totalItemCount = res.totalItemCount;
        _list = res.list;
      })
    })
        .catchError((err) =>
    {
      BotToast.closeAllLoading(),
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Scaffold(
          backgroundColor: colorSecondary,
          appBar: ComponentAppbarActionsBack(
            title: '나의문의',
          ),
          bottomNavigationBar: BottomAppBar(
            child: ComponentOutlineButton(
              callback: () {
                _loadItems();
              },
              name: '조회하기',
            ),
          ),
          body: SingleChildScrollView(
            child: Container(
              padding: bodyPaddingAll,
              margin: bodyPaddingAll,
              decoration: BoxDecoration(
                color: colorWhite,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(children: [
                FormBuilderDropdown<int>(
                  name: 'year',
                  decoration: const InputDecoration(
                    labelText: '년도',
                  ),
                  items: dropdownYear,
                  initialValue: _selectedYear,
                  onChanged: (val) {
                    setState(() {
                      _selectedYear = val!;
                    });
                    _loadItems();
                  },
                ),
                SizedBox(width: 10),
                FormBuilderDropdown<int>(
                  name: 'month',
                  decoration: const InputDecoration(
                    labelText: '월',
                  ),
                  items: dropdownMonth,
                  initialValue: _selectedMonth,
                  onChanged: (val) {
                    setState(() {
                      _selectedMonth = val!;
                    });
                    _loadItems();
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                ComponentCountTitle(
                    icon: Icons.add,
                    count: _totalItemCount,
                    unitName: '건',
                    itemName: '문의 데이터'),
                Divider(),
                _buildBody(),
              ]),
            ),
          ),
        ));
  }

  Widget _buildBody() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) =>
                ComponentComplainListItem(
                  item: _list[index],
                  callback: () async {
                    final popup = await Navigator.push(
                        context,
                        MaterialPageRoute(
                        builder: (context)
                    =>
                        PageComplainDetail(
                            complainId: _list[index].complainId)));
                  },
                ))
      ],
    );
  }
}


