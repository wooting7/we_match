import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:we_match/component/component_appbar_actions.dart';
import 'package:we_match/config/config_color.dart';
import 'package:we_match/page/page_complain_list.dart';
import 'package:we_match/page/page_complain_new.dart';
import 'package:we_match/page/page_my_info_put.dart';
import 'package:we_match/page/page_setting.dart';

import '../component/component_outline_button_login.dart';
import '../config/config_decoration.dart';
import '../config/config_size.dart';

class PageMyInfo extends StatefulWidget {
  const PageMyInfo({Key? key}) : super(key: key);

  @override
  State<PageMyInfo> createState() => _PageMyInfoState();
}

class _PageMyInfoState extends State<PageMyInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
        title: '나의정보',
        isUseActionBtn1: true,
        action1Icon: Icons.settings,
        action1Callback: () {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => PageSetting())
          );
        }),
      body: _BuildBody(),
      backgroundColor: colorSecondary,
    );
  }

  Widget _BuildBody() {
    return Scaffold(
      body:
      Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
          const SizedBox(
          height: 20,),
            Container(
                margin: EdgeInsets.all(20.0),
                padding: bodyPaddingAll,
                decoration: BoxDecoration(
                  color: colorPrimary,
                ),
                child:
                ComponentOutlineButtonLogin(callback: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PageMyInfoPut())
                  );
                }, name: '프로필 등록/변경')),
            const SizedBox(
              height: 20,
            ),
            Container(
                margin: EdgeInsets.all(20.0),
                padding: bodyPaddingAll,
                decoration: BoxDecoration(
                  color: colorPrimary,
                ),
                child:
                ComponentOutlineButtonLogin(callback: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PageComplainNew())
                  );
                }, name: '1:1 문의')),
            const SizedBox(
              height: 20,
            ),
            Container(
                margin: EdgeInsets.all(20.0),
                padding: bodyPaddingAll,
                decoration: BoxDecoration(
                  color: colorPrimary,
                ),
                child:
                ComponentOutlineButtonLogin(callback: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PageComplainList())
                  );
                }, name: '문의내역')),
          ]),
      backgroundColor: colorSecondary,
    );
  }
}
