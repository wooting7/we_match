import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';
import 'package:we_match/model/member/member_join_request.dart';
import 'package:we_match/model/member/member_update_request.dart';
import 'package:we_match/repository/repo_member.dart';

import '../component/component_appbar_actions_back.dart';
import '../component/component_custom_loading.dart';
import '../component/component_notification.dart';
import '../component/component_outline_button.dart';
import '../config/config_color.dart';
import '../config/config_decoration.dart';
import '../config/config_form_validator.dart';

class PageMemberJoin extends StatefulWidget {
  const PageMemberJoin({Key? key}) : super(key: key);

  @override
  State<PageMemberJoin> createState() => _PageMemberJoinState();
}

class _PageMemberJoinState extends State<PageMemberJoin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setMatch(MemberJoinRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().setMember(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: true,
        title: '회원가입 성공',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: false,
        title: '회원가입 실패',
        subTitle: '회원가입에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActionsBack(
        title: '회원가입',
      ),
      body: _BuildBody(),
      backgroundColor: colorSecondary,
    );
  }

  Widget _BuildBody() {
    return SingleChildScrollView(
        child: Container(
          padding: bodyPaddingAll,
          margin: bodyPaddingAll,
          decoration: BoxDecoration(
            color: colorWhite,
            borderRadius: BorderRadius.circular(10),
          ),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  FormBuilderTextField(
                    name: 'username',
                    decoration: const InputDecoration(
                        labelText: '아이디', hintText: '2~20자 입력해주세요'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2,
                          errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20,
                          errorText: formErrorMaxLength(20)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  FormBuilderTextField(
                    name: 'password',
                    decoration: const InputDecoration(
                        labelText: '비밀번호', hintText: '8~20자 입력해주세요'),
                    obscureText: true,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8,
                          errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20,
                          errorText: formErrorMaxLength(20)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  FormBuilderTextField(
                    name: 'passwordRe',
                    decoration: const InputDecoration(
                        labelText: '비밀번호 확인', hintText: '8~20자 입력해주세요'),
                    obscureText: true,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8,
                          errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20,
                          errorText: formErrorMaxLength(20)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  FormBuilderTextField(
                    name: 'nickname',
                    decoration: const InputDecoration(
                        labelText: '닉네임', hintText: '2~10자 입력해주세요'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2,
                          errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(10,
                          errorText: formErrorMaxLength(10)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  FormBuilderTextField(
                    name: 'memberName',
                    decoration: const InputDecoration(
                        labelText: '회원이름', hintText: '3~20자 입력해주세요'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2,
                          errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20,
                          errorText: formErrorMaxLength(20)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),

                  FormBuilderDateTimePicker(
                    name: 'birthday',
                    format: DateFormat('yyyy-MM-dd'),
                    inputType: InputType.date,
                    decoration: InputDecoration(
                      labelText: '생년월일',
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          _formKey.currentState!.fields['birthday']
                              ?.didChange(null);
                        },
                      ),
                    ),
                    locale: const Locale.fromSubtags(languageCode: 'ko'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired)
                    ]),
                  ),

                  FormBuilderTextField(
                    name: 'gender',
                    decoration: const InputDecoration(
                        labelText: '성별', hintText: '2~10자 입력해주세요'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2,
                          errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(10,
                          errorText: formErrorMaxLength(10)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),

                  FormBuilderTextField(
                    name: 'phone',
                    decoration: const InputDecoration(
                        labelText: '전화번호', hintText: '2~20자 입력해주세요'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2,
                          errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20,
                          errorText: formErrorMaxLength(20)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ComponentOutlineButton(
                      callback: () {
                        if (_formKey.currentState?.saveAndValidate() ?? false) {
                          MemberJoinRequest request = MemberJoinRequest(
                                         _formKey.currentState!.fields['username']!.value,
                            _formKey.currentState!.fields['password']!.value,
                            _formKey.currentState!.fields['passwordRe']!.value,
                            _formKey.currentState!.fields['nickname']!.value,
                            _formKey.currentState!.fields['memberName']!.value,
                            DateFormat('yyyy-MM-dd').format(
                                _formKey.currentState!.fields['birthday']!.value),
                            _formKey.currentState!.fields['gender']!.value,
                            _formKey.currentState!.fields['phone']!.value,
                          );
                          _setMatch(request);
                        }
                      },
                      name: '완료')
                ]),
          ),
        ));
  }
}