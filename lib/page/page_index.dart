
import 'package:flutter/material.dart';
import 'package:we_match/page/page_my_Info.dart';
import 'package:we_match/page/page_home.dart';
import 'package:we_match/page/page_my_match_List.dart';
import 'package:we_match/page/page_join_match.dart';

import '../config/config_color.dart';



class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);



  @override
  State<StatefulWidget> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  static const _kTabPages = <Widget>[
    PageHome(),
    PageMyMatchList(),
    PageJoinMatch(),
    PageMyInfo(),


  ];
  static const _kTabs = <Tab>[
    Tab(icon: Icon(Icons.home_outlined, color: colorPrimary), child: Text('홈',style: TextStyle(color: colorDarkGray),),),
    Tab(icon: Icon(Icons.calendar_today, color: colorPrimary), child: Text('나의매치',style: TextStyle(color: colorDarkGray),),),
    Tab(icon: Icon(Icons.post_add, color: colorPrimary), child: Text('참가내역',style: TextStyle(color: colorDarkGray),),),
    Tab(icon: Icon(Icons.person, color: colorPrimary), child: Text('나의정보',style: TextStyle(color: colorDarkGray),),),
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: _kTabPages.length,
      vsync: this,
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TabBarView(
        controller: _tabController,
        children: _kTabPages,
      ),
      bottomNavigationBar: Material(
        color: colorWhite,
        child: TabBar(
          tabs: _kTabs,
          controller: _tabController,
        ),
      ),
    );
  }
}