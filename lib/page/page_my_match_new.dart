import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';
import 'package:we_match/model/match/match_request.dart';
import 'package:we_match/repository/repo_match.dart';

import '../component/component_appbar_actions_back.dart';
import '../component/component_custom_loading.dart';
import '../component/component_notification.dart';
import '../component/component_outline_button.dart';
import '../config/config_color.dart';
import '../config/config_decoration.dart';
import '../config/config_form_validator.dart';

class PageMyMatchNew extends StatefulWidget {
  const PageMyMatchNew({Key? key}) : super(key: key);

  @override
  State<PageMyMatchNew> createState() => _PageMyMatchNewState();
}

class _PageMyMatchNewState extends State<PageMyMatchNew> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setMatch(MatchRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMatch().setMatch(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: true,
        title: '매칭 등록 성공',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: false,
        title: '매칭 등록 실패',
        subTitle: '매칭 등록에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActionsBack(
        title: '매칭 글쓰기',
      ),
      body: _BuildBody(),
      backgroundColor: colorSecondary,
    );
  }

  Widget _BuildBody() {
    return SingleChildScrollView(
        child: Container(
      padding: bodyPaddingAll,
      margin: bodyPaddingAll,
      decoration: BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.circular(10),
      ),
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.disabled,
        child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              FormBuilderDateTimePicker(
                name: 'sportDate',
                format: DateFormat('yyyy-MM-dd'),
                inputType: InputType.date,
                decoration: InputDecoration(
                  labelText: '경기일',
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: () {
                      _formKey.currentState!.fields['sportDate']
                          ?.didChange(null);
                    },
                  ),
                ),
                locale: const Locale.fromSubtags(languageCode: 'ko'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired)
                ]),
              ),
              FormBuilderTextField(
                name: 'sportType',
                decoration: const InputDecoration(
                    labelText: '주종목', hintText: '2~20자 입력해주세요'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(20,
                      errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.text,
              ),
              const SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                name: 'title',
                decoration: const InputDecoration(
                    labelText: '매칭제목', hintText: '2~30자 입력해주세요'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(30,
                      errorText: formErrorMaxLength(30)),
                ]),
                keyboardType: TextInputType.text,
              ),
              const SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                maxLength: 300,
                maxLines: 10,
                name: 'contents',
                decoration: const InputDecoration(
                    labelText: '글 내용', hintText: '2~300자 입력해주세요'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(300,
                      errorText: formErrorMaxLength(300)),
                ]),
                keyboardType: TextInputType.text,
              ),
              const SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                name: 'placeName',
                decoration: const InputDecoration(
                    labelText: '경기장소', hintText: '2~30자 입력해주세요'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(30,
                      errorText: formErrorMaxLength(30)),
                ]),
                keyboardType: TextInputType.text,
              ),
              const SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                name: 'addressName',
                decoration: const InputDecoration(
                    labelText: '상세주소', hintText: '2~40자 입력해주세요'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(40,
                      errorText: formErrorMaxLength(40)),
                ]),
                keyboardType: TextInputType.text,
              ),
              const SizedBox(
                height: 10,
              ),
              ComponentOutlineButton(
                  callback: () {
                    if (_formKey.currentState?.saveAndValidate() ?? false) {
                      MatchRequest request = MatchRequest(
                        DateFormat('yyyy-MM-dd').format(
                            _formKey.currentState!.fields['sportDate']!.value),
                        _formKey.currentState!.fields['sportType']!.value,
                        _formKey.currentState!.fields['title']!.value,
                        _formKey.currentState!.fields['contents']!.value,
                        _formKey.currentState!.fields['placeName']!.value,
                        _formKey.currentState!.fields['addressName']!.value,
                      );
                      _setMatch(request);
                    }
                  },
                  name: '완료')
            ]),
      ),
    ));
  }
}
