import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:we_match/model/member/member_update_request.dart';
import 'package:we_match/repository/repo_member.dart';

import '../component/component_appbar_actions_back.dart';
import '../component/component_custom_loading.dart';
import '../component/component_notification.dart';
import '../component/component_outline_button.dart';
import '../config/config_color.dart';
import '../config/config_decoration.dart';
import '../config/config_form_validator.dart';

class PageMyInfoPut extends StatefulWidget {
  const PageMyInfoPut({Key? key}) : super(key: key);

  @override
  State<PageMyInfoPut> createState() => _PageMyInfoPutState();
}

class _PageMyInfoPutState extends State<PageMyInfoPut> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _putMemberUpdate(MemberUpdateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().putMemberUpdate(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: true,
        title: '프로필 변경 성공',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: false,
        title: '프로필 변경 실패',
        subTitle: '프로필 변경에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActionsBack(
        title: '프로필 변경',
      ),
      body: _BuildBody(),
      backgroundColor: colorSecondary,
    );
  }

  Widget _BuildBody() {
    return SingleChildScrollView(
        child: Container(
      padding: bodyPaddingAll,
      margin: bodyPaddingAll,
      decoration: BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.circular(10),
      ),
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.disabled,
        child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              FormBuilderTextField(
                name: 'imageName',
                decoration: const InputDecoration(
                    labelText: '이미지이름', hintText: '파일을 첨부해주세요'),
                keyboardType: TextInputType.text,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                ]),
              ),
              const SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                name: 'nickname',
                decoration: const InputDecoration(
                    labelText: '닉네임', hintText: '2~10자 입력해주세요'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(10,
                      errorText: formErrorMaxLength(10)),
                ]),
                keyboardType: TextInputType.text,
              ),
              const SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                name: 'location',
                decoration: const InputDecoration(
                    labelText: '활동지', hintText: '2~2 입력해주세요'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(20,
                      errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.text,
              ),
              const SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                name: 'sportType',
                decoration: const InputDecoration(
                    labelText: '주종목', hintText: '2~15자 입력해주세요'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(15,
                      errorText: formErrorMaxLength(15)),
                ]),
                keyboardType: TextInputType.text,
              ),
              const SizedBox(
                height: 10,
              ),
              ComponentOutlineButton(
                  callback: () {
                    if (_formKey.currentState?.saveAndValidate() ?? false) {
                      MemberUpdateRequest request = MemberUpdateRequest(
                        _formKey.currentState!.fields['imageName']!.value,
                        _formKey.currentState!.fields['nickname']!.value,
                        _formKey.currentState!.fields['location']!.value,
                        _formKey.currentState!.fields['sportType']!.value,
                      );
                      _putMemberUpdate(request);
                    }
                  },
                  name: '프로필 변경')
            ]),
      ),
    ));
  }
}
