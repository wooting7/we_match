import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:we_match/component/component_appbar_actions_back.dart';
import 'package:we_match/component/component_count_title_poeple.dart';
import 'package:we_match/component/component_custom_loading.dart';
import 'package:we_match/component/component_list_item_row.dart';
import 'package:we_match/component/component_my_match_apply_list_item.dart';
import 'package:we_match/component/component_notification.dart';
import 'package:we_match/component/component_outline_button.dart';
import 'package:we_match/config/config_color.dart';
import 'package:we_match/config/config_decoration.dart';
import 'package:we_match/model/match/match_apply_item.dart';
import 'package:we_match/model/match/match_detail.dart';
import 'package:we_match/repository/repo_match.dart';
import 'package:we_match/repository/repo_match_apply.dart';

class PageMyMatchDetail extends StatefulWidget {
  const PageMyMatchDetail({super.key, required this.matchId});

  final int matchId;

  @override
  State<PageMyMatchDetail> createState() => _PageMyMatchDetailState();
}

class _PageMyMatchDetailState extends State<PageMyMatchDetail> {
  List<MatchApplyItem> _list = [];
  MatchDetail _detail = MatchDetail(0, '', '', '', '', '', '', 0, '', '');
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _matchDetail();
    _getMatchApplyList('YET');
  }

  Future<void> _matchDetail() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMatch().getMatchDetail(widget.matchId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _detail = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      Navigator.pop(context);
    });
  }

  Future<void> _getMatchApplyList(String acceptState) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMatchApply()
        .getMyMatchList(widget.matchId, acceptState)
        .then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _totalItemCount = res.totalItemCount;
        _list = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorSecondary,
      appBar: ComponentAppbarActionsBack(
        title: '나의매치상세',
      ),
      body: SingleChildScrollView(
        child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                padding: bodyPaddingLeftRight,
                color: colorWhite,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    ComponentOutlineButton(callback: () {}, name: '수정'),
                    SizedBox(
                      width: 10,
                    ),
                    ComponentOutlineButton(callback: () {}, name: '삭제'),
                  ],
                ),
              ),
              Container(
                transformAlignment: Alignment.topLeft,
                padding: bodyPaddingAll,
                margin: bodyPaddingAll,
                decoration: BoxDecoration(
                  color: colorWhite,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('${_detail.title}',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Text('${_detail.addressName}'),
                      Container(
                        child: ComponentListITemRow(
                            name: '경기일: ${_detail.sportDate}',
                            variableName: '생성일: ${_detail.dateCreate}'),
                      ),
                      Divider(),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        '${_detail.contents}내용',
                        maxLines: 10,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Divider(),
                      ComponentCountTitlePeople(
                          count: _totalItemCount,
                          unitName: '명',
                          itemName: '인원'),
                      _buildList(),
                    ]),
              ),
            ]),
      ),
    );
  }

  Widget _buildList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentMyMatchApplyListItem(
              item: _list[index],
              callback: () {},
            ),
          ),
        ],
      ),
    );
  }
}
