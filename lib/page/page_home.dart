import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:we_match/component/component_appbar_filter.dart';
import 'package:we_match/component/component_count_title.dart';
import 'package:we_match/component/component_custom_loading.dart';
import 'package:we_match/component/component_home_list_item.dart';
import 'package:we_match/config/config_color.dart';
import 'package:we_match/model/match/match_item.dart';
import 'package:we_match/page/page_home_detail.dart';
import 'package:we_match/page/page_home_search.dart';
import 'package:we_match/page/page_my_match_new.dart';
import 'package:we_match/repository/repo_match.dart';

class PageHome extends StatefulWidget {
  const PageHome({Key? key}) : super(key: key);

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome>
    with AutomaticKeepAliveClientMixin {
  final _scrollController = ScrollController();

  List<MatchItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  String _title = '';

  @override
  void initState() {
    super.initState();
    _getMatchList();
  }

  Future<void> _getMatchList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMatch().getMatchList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _totalItemCount = res.totalItemCount;
        _list = res.list;
      });
    }).catchError((err) => BotToast.closeAllLoading());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ComponentAppbarFilter(
          title: '홈',
          actionIcon: Icons.search,
          callback: () async {
            final searchResult = await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PageHomeSearch(
                          title: _title,
                        )));

            if (searchResult != null && searchResult[0]) {
              _title = searchResult[1];
              _getMatchList();
            }
          },
        ),
        backgroundColor: colorSecondary,
        body: ListView(
          controller: _scrollController,
          children: [
            SizedBox(
              width: 10,
            ),
            Container(
              decoration: BoxDecoration(
                color: colorWhite,
              ),
              child: ComponentCountTitle(
                  icon: Icons.add,
                  count: _totalItemCount,
                  unitName: '건',
                  itemName: '매치'),
            ),
            _buildBody(),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: colorPrimary,
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => PageMyMatchNew()));
          },
          child: const Icon(Icons.add),
        ));
  }

  Widget _buildBody() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentHomeListItem(
                  item: _list[index],
                  callback: () async {
                    final popup = await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                PageHomeDetail(matchId: _list[index].matchId)));

                    // 상세페이지에서 수정이나 삭제 완료하고 닫혔다!! 라고 알려주면 리스트 갱신하기
                    if (popup != null && popup[0]) {
                      _getMatchList();
                    }
                  },
                ))
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
