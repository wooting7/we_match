import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:we_match/component/component_appbar_actions.dart';
import 'package:we_match/component/component_count_title.dart';
import 'package:we_match/component/component_custom_loading.dart';
import 'package:we_match/component/component_my_apply_matches.dart';
import 'package:we_match/config/config_color.dart';
import 'package:we_match/model/match/match_apply_item.dart';
import 'package:we_match/page/page_join_match_detail.dart';
import 'package:we_match/repository/repo_match_apply.dart';


class PageJoinMatch extends StatefulWidget {
  const PageJoinMatch({Key? key}) : super(key: key);

  @override
  State<PageJoinMatch> createState() => _PageJoinMatchState();
}

class _PageJoinMatchState extends State<PageJoinMatch> {
  List<MatchApplyItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _loadItems();
  }

  Future<void> _loadItems() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMatchApply()
        .getMyApplyMatches()
        .then((res) =>
    {
      BotToast.closeAllLoading(),
      setState(() {
        _totalItemCount = res.totalItemCount;
        _list = res.list;
      })
    })
        .catchError((err) =>
    {
      BotToast.closeAllLoading(),
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Scaffold(
          backgroundColor: colorSecondary,
          appBar: ComponentAppbarActions(
            title: '참가내역',
          ),
          body: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: colorWhite,
                    ),
                    child: Column(children: [
                      ComponentCountTitle(
                          icon: Icons.add,
                          count: _totalItemCount,
                          unitName: '건',
                          itemName: '매치'),
                    ]),
                  ),
                  _buildBody(),
                ],
              )),
        ));
  }

  Widget _buildBody() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) =>
                ComponentMyApplyMatches(
                  item: _list[index],
                  callback: () async {
                    final popup = await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                PageJoinMatchDetail(
                                    matchId: _list[index].matchId,
                                acceptState: _list[index].acceptStateName,)));

                    // 상세페이지에서 수정이나 삭제 완료하고 닫혔다!! 라고 알려주면 리스트 갱신하기
                    if (popup != null && popup[0]) {
                      _loadItems();
                    }
                  },
                ))
      ],
    );
  }
}
