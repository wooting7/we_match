
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:we_match/page/page_member_join.dart';

import '../component/component_custom_loading.dart';
import '../component/component_notification.dart';
import '../component/component_outline_button_login.dart';
import '../config/config_color.dart';
import '../config/config_decoration.dart';
import '../config/config_form_validator.dart';
import '../config/config_size.dart';
import '../functions/token_lib.dart';
import '../middleware/middleware_login_check.dart';
import '../model/login/login_request.dart';
import '../repository/repo_member.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(LoginRequest loginRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().doLogin(loginRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '로그인 성공',
        subTitle: '로그인에 성공하였습니다.',
      ).call();

      // api에서 받아온 결과값을 token에 넣는다.
      TokenLib.setToken(res.data.token);
      print(res.data.token);
      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddlewareLoginCheck().check(context);

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '로그인 실패',
        subTitle: '아이디 혹은 비밀번호를 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
      backgroundColor: colorWhite,

    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.disabled,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(
              height: 100,
            ),
            Container(
              margin: EdgeInsets.all(20.0),
              alignment: Alignment.center,
              child: Text('위 매 치', style: TextStyle(color: colorPrimary, fontSize: fontSizeSuper, fontWeight: FontWeight.bold) ),
                ),
            const SizedBox(
              height: 20,
            ),
            Container(
              margin: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                color: colorSecondary,

              ),
              child: FormBuilderTextField(
                name: 'username',
                decoration: const InputDecoration(
                  labelText: '아이디',
                  hintText: '2~20자 입력해주세요',
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                    icon: Icon(Icons.login_outlined)
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)),
                  FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.text,
              ),

            ),

          Container(

            margin: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              color: colorSecondary,

            ),
            child: FormBuilderTextField(
              name: 'password',
              decoration: const InputDecoration(
                labelText: '비밀번호',
                  hintText: '8~20자 입력해주세요',
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  icon: Icon(Icons.logout)

              ),
              obscureText: true,
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(errorText: formErrorRequired),
                FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
              ]),
              keyboardType: TextInputType.text,
            ),
          ),


        Container(
            margin: EdgeInsets.all(20.0),
            padding: bodyPaddingAll,
            decoration: BoxDecoration(
              color: colorPrimary,

            ),
            child:
            ComponentOutlineButtonLogin(
                callback: () {
                  if (_formKey.currentState?.saveAndValidate() ?? false) {
                    LoginRequest loginRequest = LoginRequest(
                      _formKey.currentState!.fields['username']!.value,
                      _formKey.currentState!.fields['password']!.value,
                    );
                    _doLogin(loginRequest);
                  }
                },
                name: '로그인')
        ),

            Container(
                margin: EdgeInsets.all(20.0),
                padding: bodyPaddingAll,
                decoration: BoxDecoration(
                  color: colorPrimary,

                ),
                child:
                ComponentOutlineButtonLogin(
                    callback: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageMemberJoin())
                      );
                    },

                    name: '회원가입')
            ),

          ],
        ),
      ),
    );
  }



}
