class LoginResponse {
  String token;
  String username;



  LoginResponse(this.token, this.username);


  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
      json['token'],
      json['username'],
    );
  }
}