
class MemberUpdateRequest {

  String imageName;
  String nickname;
  String location;
  String sportType;

  MemberUpdateRequest(
      this.imageName,
      this.nickname,
      this.location,
      this.sportType,
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['imageName'] = this.imageName;
    data['nickname'] = this.nickname;
    data['location'] = this.location;
    data['contents'] = this.sportType;

    return data;
  }
}