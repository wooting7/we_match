class MemberJoinRequest {
  String username;
  String password;
  String passwordRe;
  String nickname;
  String memberName;
  String birthday;
  String gender;
  String phone;

  MemberJoinRequest(this.username, this.password, this.passwordRe, this.nickname, this.memberName, this.birthday, this.gender, this.phone);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['username'] = this.username;
    data['password'] = this.password;
    data['passwordRe'] = this.passwordRe;
    data['nickname'] = this.nickname;
    data['memberName'] = this.memberName;
    data['birthday'] = this.birthday;
    data['gender'] = this.gender;
    data['phone'] = this.phone;

    return data;

  }
}