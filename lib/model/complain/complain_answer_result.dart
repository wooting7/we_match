

import 'package:we_match/model/complain/complain_answer_item.dart';

class ComplainAnswerResult {
  List<ComplainAnswerItem> list;
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;

  ComplainAnswerResult(this.list, this.isSuccess, this.code, this.msg, this.totalItemCount, this.totalPage, this.currentPage);

  factory ComplainAnswerResult.fromJson(Map<String, dynamic> json) {
    return ComplainAnswerResult(
      (json['list'] as List).map((e) => ComplainAnswerItem.fromJson(e)).toList(),
      json['isSuccess'],
      json['code'],
      json['msg'],
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
    );
  }
}