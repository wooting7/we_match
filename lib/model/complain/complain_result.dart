
import 'package:we_match/model/complain/complain_item.dart';

class ComplainResult {
  List<ComplainItem> list;
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;

  ComplainResult(this.list, this.isSuccess, this.code, this.msg, this.totalItemCount, this.totalPage, this.currentPage);

  factory ComplainResult.fromJson(Map<String, dynamic> json) {
    return ComplainResult(
      (json['list'] as List).map((e) => ComplainItem.fromJson(e)).toList(),
      json['isSuccess'],
      json['code'],
      json['msg'],
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
    );
  }
}