class ComplainAnswerItem {
  int complainAnswerId;
  int complainId;
  int memberId;
  String contents;
  String dateCreate;

  ComplainAnswerItem(
      this.complainAnswerId,
      this.complainId,
      this.memberId,
      this.contents,
      this.dateCreate,
      );

  factory ComplainAnswerItem.fromJson(Map<String, dynamic> json) {
    return ComplainAnswerItem(
      json['complainAnswerId'],
      json['complainId'],
      json['memberId'],
      json['contents'],
      json['dateCreate'],

    );
  }
}