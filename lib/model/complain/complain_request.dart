class ComplainRequest {
  String title;
  String contents;

  ComplainRequest(this.title, this.contents);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['title'] = this.title;
    data['contents'] = this.contents;

    return data;
  }
}