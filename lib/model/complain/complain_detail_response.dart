
import 'package:we_match/model/complain/complain_detail.dart';

class ComplainDetailResponse {
  ComplainDetail data;
  bool isSuccess;
  int code;
  String msg;

  ComplainDetailResponse(this.data, this.isSuccess, this.code, this.msg);

  factory ComplainDetailResponse.fromJson(Map<String, dynamic> json) {
    return ComplainDetailResponse(
      ComplainDetail.fromJson(json['data']),
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
