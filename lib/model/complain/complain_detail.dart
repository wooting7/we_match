class ComplainDetail {
  int complainId;
  String title;
  String contents;
  String processStateName;
  String dateCreate;

  ComplainDetail(
      this.complainId,
      this.title,
      this.contents,
      this.processStateName,
      this.dateCreate,
      );

  factory ComplainDetail.fromJson(Map<String, dynamic> json) {
    return ComplainDetail(
      json['complainId'],
      json['title'],
      json['contents'],
      json['processStateName'],
      json['dateCreate'],

    );
  }
}