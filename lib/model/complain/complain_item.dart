class ComplainItem {
  int complainId;
  String title;
  String processState;
  String processStateName;
  String dateCreate;
  int memberId;
  String memberUsername;


  ComplainItem(
      this.complainId,
      this.title,
      this.processState,
      this.processStateName,
      this.dateCreate,
      this.memberId,
      this.memberUsername,
      );

  factory ComplainItem.fromJson(Map<String, dynamic> json) {
    return ComplainItem(
      json['complainId'],
      json['title'],
      json['processState'],
      json['processStateName'],
      json['dateCreate'],
      json['memberId'],
      json['memberUsername'],

    );
  }
}