class MatchItem {
  int matchId;
  String placeName;
  String addressName;
  String sportType;
  String title;
  int peopleCountAccept;
  int peopleCountStandby;
  String dateCreate;
  String sportDate;
  int memberId;
  String memberNickname;

  MatchItem(
      this.matchId,
      this.placeName,
      this.addressName,
      this.sportType,
      this.title,
      this.peopleCountAccept,
      this.peopleCountStandby,
      this.dateCreate,
      this.sportDate,
      this.memberId,
      this.memberNickname,
      );

  factory MatchItem.fromJson(Map<String, dynamic> json) {
    return MatchItem(
      json['matchId'],
      json['placeName'],
      json['addressName'],
      json['sportType'],
      json['title'],
      json['peopleCountAccept'],
      json['peopleCountStandby'],
      json['dateCreate'],
      json['sportDate'],
      json['memberId'],
      json['memberNickname'],

    );
  }
}