
class MatchRequest {

  String sportDate;
  String sportType;
  String title;
  String contents;
  String placeName;
  String addressName;

  MatchRequest(
      this.sportDate,
      this.sportType,
      this.title,
      this.contents,
      this.placeName,
      this.addressName,
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['sportDate'] = this.sportDate;
    data['sportType'] = this.sportType;
    data['title'] = this.title;
    data['contents'] = this.contents;
    data['placeName'] = this.placeName;
    data['addressName'] = this.addressName;

    return data;
  }
}