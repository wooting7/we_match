class MatchDetail {
  int matchId;
  String addressName;
  String placeName;
  String sportDate;
  String title;
  String contents;
  String dateCreate;
  int memberId;
  String memberNickname;
  String isMine;

  MatchDetail(
      this.matchId,
      this.addressName,
      this.placeName,
      this.sportDate,
      this.title,
      this.contents,
      this.dateCreate,
      this.memberId,
      this.memberNickname,
      this.isMine,
      );

  factory MatchDetail.fromJson(Map<String, dynamic> json) {
    return MatchDetail(
      json['matchId'],
      json['addressName'],
      json['placeName'],
      json['sportDate'],
      json['title'],
      json['contents'],
      json['dateCreate'],
      json['memberId'],
      json['memberNickname'],
      json['isMine'],

    );
  }
}