
import 'package:we_match/model/match/match_item.dart';

class MatchResult {
  List<MatchItem> list;
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;

  MatchResult(this.list, this.isSuccess, this.code, this.msg, this.totalItemCount, this.totalPage, this.currentPage);

  factory MatchResult.fromJson(Map<String, dynamic> json) {
    return MatchResult(
      (json['list'] as List).map((e) => MatchItem.fromJson(e)).toList(),
      json['isSuccess'],
      json['code'],
      json['msg'],
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
    );
  }
}