class MatchApplyItem {
  int id;
  String acceptStateName;
  String addressName;
  int memberId;
  String memberNickname;
  int matchId;
  String title;
  int peopleCount;
  String sportDate;
  String location;

  MatchApplyItem(
      this.id,
      this.acceptStateName,
      this.addressName,
      this.memberId,
      this.memberNickname,
      this.matchId,
      this.title,
      this.peopleCount,
      this.sportDate,
      this.location,
      );

  factory MatchApplyItem.fromJson(Map<String, dynamic> json) {
    return MatchApplyItem(
      json['id'],
      json['acceptStateName'],
      json['addressName'],
      json['memberId'],
      json['memberNickname'],
      json['matchId'],
      json['title'],
      json['peopleCount'],
      json['sportDate'],
      json['location'] ?? '-',

    );
  }
}