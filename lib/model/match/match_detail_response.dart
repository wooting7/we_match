
import 'package:we_match/model/match/match_detail.dart';

class MatchDetailResponse {
  MatchDetail data;
  bool isSuccess;
  int code;
  String msg;

  MatchDetailResponse(this.data, this.isSuccess, this.code, this.msg);

  factory MatchDetailResponse.fromJson(Map<String, dynamic> json) {
    return MatchDetailResponse(
      MatchDetail.fromJson(json['data']),
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
