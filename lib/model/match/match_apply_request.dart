
class MatchApplyRequest {

  int matchId;

  MatchApplyRequest(
      this.matchId,
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['matchId'] = this.matchId;

    return data;
  }
}