
import 'package:we_match/model/match/match_apply_item.dart';

class MatchApplyResult {
  List<MatchApplyItem> list;
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;

  MatchApplyResult(this.list, this.isSuccess, this.code, this.msg, this.totalItemCount, this.totalPage, this.currentPage);

  factory MatchApplyResult.fromJson(Map<String, dynamic> json) {
    return MatchApplyResult(
      (json['list'] as List).map((e) => MatchApplyItem.fromJson(e)).toList(),
      json['isSuccess'],
      json['code'],
      json['msg'],
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
    );
  }
}