class DateSearchRequest {

  int dateStandardYear;
  int dateStandardMonth;

  DateSearchRequest(this.dateStandardYear, this.dateStandardMonth);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = <String, dynamic> {};

    data['dateStandardYear'] = this.dateStandardYear;
    data['dateStandardMonth'] = this.dateStandardMonth;

    return data;
  }
}