import 'package:flutter/material.dart';
import 'package:we_match/component/component_list_item_row.dart';
import 'package:we_match/config/config_color.dart';
import 'package:we_match/config/config_decoration.dart';
import 'package:we_match/model/match/match_item.dart';

class ComponentHomeListItem extends StatelessWidget {
  const ComponentHomeListItem(
      {super.key, required this.item, required this.callback});

  final MatchItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        width: 300,
        padding: bodyPaddingAll,
        margin: contentPaddingButton,
        decoration: BoxDecoration(
          color: colorWhite,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Image.asset(
                'assets/match_img.png',
                width: 80,
                height: 80,
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Container(
                  width: 200,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Flexible(
                          child: RichText(
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            strutStyle: StrutStyle(fontSize: 16.0),
                            text: TextSpan(
                                text:
                                '${item.title}', style: TextStyle(color: Colors.black,
                                fontSize: 16.0,
                                fontFamily: 'NanumSquareRegular')),
                          )),
                    ],
                  )),
              SizedBox(
                height: 10,
              ),
              Text('경기일: ${item.sportDate}'),
              Text('${item.placeName}'),
              ComponentListITemRow(
                  name: '현재인원: ${item.peopleCountAccept}',
                  variableName: '신청인원: ${item.peopleCountStandby}'),
            ]),
          ],
        ),
      ),
    );
  }
}
