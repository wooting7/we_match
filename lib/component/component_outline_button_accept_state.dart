
import 'package:flutter/material.dart';
import 'package:we_match/config/config_color.dart';


class ComponentOutlineButtonAcceptState extends StatelessWidget {
  const ComponentOutlineButtonAcceptState({super.key, required this.callback, required this.name});

  final VoidCallback callback;
  final String name;

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
        onPressed: callback,
        child: Text(name, style: TextStyle(color:colorPrimary )),
        style: OutlinedButton.styleFrom(
          backgroundColor: colorWhite,
        )

    );
  }
}
