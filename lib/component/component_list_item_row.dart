import 'package:flutter/material.dart';

class ComponentListITemRow extends StatelessWidget {
  const ComponentListITemRow({super.key,
  required this.name,
  required this.variableName});

  final String name;
  final String variableName;
  



  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [    //MediaQuery.of(context).size.width - 40 - 20,
        Container(
          child: Text(name,style: TextStyle(),),
        ),
        const SizedBox(width: 10,),
        Text(variableName, style: TextStyle(color: Colors.grey))
      ],

    );
  }
}
