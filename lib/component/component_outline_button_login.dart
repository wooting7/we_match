
import 'package:flutter/material.dart';

import '../config/config_color.dart';

class ComponentOutlineButtonLogin extends StatelessWidget {
  const ComponentOutlineButtonLogin({super.key, required this.callback, required this.name});

  final VoidCallback callback;
  final String name;

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: callback,

        child: Text(name, style: TextStyle(color:colorWhite )),


        );


  }
}
