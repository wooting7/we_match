import 'package:flutter/material.dart';
import 'package:we_match/model/complain/complain_item.dart';

import '../config/config_color.dart';
import '../config/config_decoration.dart';

class ComponentComplainListItem extends StatelessWidget {
  const ComponentComplainListItem(
      {super.key, required this.item, required this.callback});

  final ComplainItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: callback,
        child: Container(
          padding: bodyPaddingLeftRight,
          decoration: BoxDecoration(
            color: colorWhite,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text('번호: ${item.complainId}',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              Container(
                  width: 200,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Flexible(
                          child: RichText(
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            strutStyle: StrutStyle(fontSize: 16.0),
                            text: TextSpan(
                                text:
                                '${item.title}', style: TextStyle(color: Colors.black,
                                fontSize: 16.0,
                                fontFamily: 'NanumSquareRegular')),
                          )),
                    ],
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('${item.processStateName}'),
                  Text('작성일: ${item.dateCreate}',
                      style: TextStyle(color: colorGray)),
                ],
              ),
              Divider(),
            ],
          ),
        ));
  }
}
