import 'package:flutter/material.dart';
import 'package:we_match/component/component_list_item_row.dart';
import 'package:we_match/config/config_color.dart';
import 'package:we_match/config/config_decoration.dart';
import 'package:we_match/model/match/match_apply_item.dart';

class ComponentMyApplyMatches extends StatelessWidget {
  const ComponentMyApplyMatches(
      {super.key, required this.item, required this.callback});

  final MatchApplyItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: callback,
        child: Container(
          padding: bodyPaddingAll,
          margin: bodyPaddingAll,
          decoration: BoxDecoration(
            color: colorWhite,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                child: ComponentListITemRow(
                    name: 'No: ${item.matchId}',
                    variableName: '상태: ${item.acceptStateName}'),
              ),
              Container(
                child: ComponentListITemRow(
                    name: '총인원: ${item.peopleCount}',
                    variableName: '경기일: ${item.sportDate}'),
              ),
              Container(
                  width: 200,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Flexible(
                          child: RichText(
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        strutStyle: StrutStyle(fontSize: 16.0),
                        text: TextSpan(
                            text: '${item.title}',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16.0,
                                fontFamily: 'NanumSquareRegular')),
                      )),
                    ],
                  )),
            ],
          ),
        ));
  }
}
