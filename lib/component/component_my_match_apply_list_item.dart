import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:we_match/component/component_custom_loading.dart';
import 'package:we_match/component/component_notification.dart';
import 'package:we_match/component/component_outline_button_accept_state.dart';
import 'package:we_match/config/config_color.dart';
import 'package:we_match/config/config_decoration.dart';
import 'package:we_match/model/match/match_apply_item.dart';
import 'package:we_match/repository/repo_match_apply.dart';


class ComponentMyMatchApplyListItem extends StatelessWidget {
  const ComponentMyMatchApplyListItem({
    super.key,
    required this.item,
    required this.callback
  });

  final MatchApplyItem item;
  final VoidCallback callback;


  Future<void> _putMatchApplyState(int matchApplyId, String acceptState) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMatchApply().putMatchApplyState(matchApplyId, acceptState).then((res) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: true,
        title: '수락상태 변경 성공',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: false,
        title: '수락상태 변경 실패',
        subTitle: '수락상태 변경에 실패하였습니다.',
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        padding: bodyPaddingAll,
        margin: contentPaddingButton,
        decoration: BoxDecoration(
          color: colorLightGray,
          borderRadius: BorderRadius.circular(10),
        ),
        child:Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('닉네임: ${item.memberNickname}',
                    style: TextStyle(fontWeight: FontWeight.bold)),
                Text('주소: ${item.location}'),

              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  child: ComponentOutlineButtonAcceptState(callback: () {
                    _putMatchApplyState(item.id, 'ACCEPT');
                  }, name: '수락'),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  child: ComponentOutlineButtonAcceptState(callback: () {
                    _putMatchApplyState(item.id, 'REFUSE');
                  }, name: '거절'),
                )
              ],
            )],

        )


      ),
    );
  }
}
