
import 'package:flutter/material.dart';
import 'package:we_match/config/config_color.dart';
import 'package:we_match/config/config_decoration.dart';
import 'package:we_match/model/match/match_apply_item.dart';


class ComponentJoinMatchDetailListItem extends StatelessWidget {
  const ComponentJoinMatchDetailListItem({
    super.key,
    required this.item,
    required this.callback
  });

  final MatchApplyItem item;
  final VoidCallback callback;



  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
          padding: bodyPaddingAll,
          margin: contentPaddingButton,
          decoration: BoxDecoration(
            color: colorLightGray,
            borderRadius: BorderRadius.circular(10),
          ),
          child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('닉네임: ${item.memberNickname}',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  Text('주소: ${item.location}'),
                  Text('연락처: 010-0000-0001'),
                ],
              ),


          ])


      ),
    );
  }
}
