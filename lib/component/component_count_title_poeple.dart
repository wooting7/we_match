import 'package:flutter/material.dart';

class ComponentCountTitlePeople extends StatelessWidget {
  const ComponentCountTitlePeople({
    super.key,
    required this.count,
    required this.unitName,
    required this.itemName
  });

  final int count;
  final String unitName;
  final String itemName;


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          const SizedBox(width: 5,),
          Text(
            '총 ${count.toString()}$unitName의 $itemName이 있습니다.',
            style: const TextStyle(
            ),
          ),
        ],
      ),
    );
  }
}
