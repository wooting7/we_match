import 'package:flutter/material.dart';
import 'package:we_match/model/complain/complain_answer_item.dart';

import '../config/config_color.dart';
import '../config/config_decoration.dart';

class ComponentComplainAnswerListItem extends StatelessWidget {
  const ComponentComplainAnswerListItem({
    super.key,
    required this.item,
  });

  final ComplainAnswerItem item;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
          padding: bodyPaddingAll,
          margin: contentPaddingButton,
          decoration: BoxDecoration(
            color: colorLightGray,
            borderRadius: BorderRadius.circular(10),
          ),
          child:
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('내용: \n ${item.contents}',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text('작성일: ${item.dateCreate}'),
                    ],
                  )


                ],
              ),



      ),
    );
  }
}
