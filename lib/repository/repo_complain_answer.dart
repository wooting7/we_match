
import 'package:dio/dio.dart';
import 'package:we_match/config/config_api.dart';
import 'package:we_match/functions/token_lib.dart';
import 'package:we_match/model/complain/complain_answer_result.dart';

class RepoComplainAnswer {

  Future<ComplainAnswerResult> getComplainAnswerList(int complainId) async {
    const String baseUrl = '$apiUrl/complain/common/list/complain-id/{complainId}';
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    final response = await dio.get(
      baseUrl.replaceAll('{complainId}', complainId.toString()),
      options: Options(

        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return ComplainAnswerResult.fromJson(response.data);
  }

}