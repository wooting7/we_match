

import 'package:dio/dio.dart';
import 'package:we_match/config/config_api.dart';
import 'package:we_match/functions/token_lib.dart';
import 'package:we_match/model/common_result.dart';
import 'package:we_match/model/match/match_apply_result.dart';

class RepoMatchApply {
  Future<CommonResult> setMatchApply(int matchId) async {
    const String baseUrl = '$apiUrl/match/user/apply/match-id/{matchId}';
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.post(
        baseUrl.replaceAll('{matchId}', matchId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<MatchApplyResult> getMyMatchList(int matchId, String acceptState) async {
    const String baseUrl = '$apiUrl/match/user/apply/list/match-id/{matchId}/state/{acceptState}';
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    final response = await dio.get(
      baseUrl.replaceAll('{matchId}', matchId.toString()).replaceAll('{acceptState}', acceptState),
      options: Options(

        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return MatchApplyResult.fromJson(response.data);
  }

  Future<CommonResult> putMatchApplyState(int matchApplyId,
      String acceptState) async {
    const String baseUrl = '$apiUrl/match/user/apply/match-apply-id/{matchApplyId}/state/{acceptState}';
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    try {
      final response = await dio.put(
        baseUrl.replaceAll('{matchApplyId}', matchApplyId.toString())
            .replaceAll('{acceptState}', acceptState),
        options: Options(

          followRedirects: false,
          validateStatus: (status) => (status == 200),
        ),
      );

      return CommonResult.fromJson(response.data);
    } on DioError catch (e) {
      return CommonResult.fromJson(e.response!.data);
    }
  }

  Future<MatchApplyResult> getMyApplyMatches() async {
    const String baseUrl = '$apiUrl/match/user/apply/list';
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    final response = await dio.get(
      baseUrl,
      options: Options(

        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return MatchApplyResult.fromJson(response.data);
  }

  Future<MatchApplyResult> getMyMatchApplies(int matchId, String acceptState) async {
    const String baseUrl = '$apiUrl/match/user/apply/list/match-id/{matchId}/state/{acceptState}';
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    final response = await dio.get(
      baseUrl.replaceAll('{matchId}', matchId.toString())
          .replaceAll('{acceptState}', acceptState),
      options: Options(

        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return MatchApplyResult.fromJson(response.data);
  }



}