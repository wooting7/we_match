import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:we_match/config/config_api.dart';
import 'package:we_match/functions/token_lib.dart';
import 'package:we_match/model/common_result.dart';
import 'package:we_match/model/complain/complain_detail_response.dart';
import 'package:we_match/model/complain/complain_request.dart';
import 'package:we_match/model/complain/complain_result.dart';

class RepoComplain {
  Future<CommonResult> setComplain(ComplainRequest request) async {
    const String baseUrl = '$apiUrl/complain/user/new';
    String? token = await TokenLib.getToken();
    var test = request.toJson();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<ComplainResult> getMyComplainList(int year, int month) async {
    const String baseUrl = '$apiUrl/complain/user/list/year/{year}/month/{month}';
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    final response = await dio.get(
      baseUrl.replaceAll('{year}', year.toString()).replaceAll('{month}', month.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return ComplainResult.fromJson(response.data);
  }

  Future<ComplainDetailResponse> getMatchDetail(int complainId) async {
    const String baseUrl = '$apiUrl/complain/user/detail/complain-id/{complainId}';
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    final response = await dio.get(
      baseUrl.replaceAll('{complainId}', complainId.toString()),
      options: Options(

        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return ComplainDetailResponse.fromJson(response.data);
  }


}