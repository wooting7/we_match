import 'package:dio/dio.dart';
import 'package:we_match/config/config_api.dart';
import 'package:we_match/functions/token_lib.dart';
import 'package:we_match/model/common_result.dart';
import 'package:we_match/model/login/login_request.dart';
import 'package:we_match/model/login/login_result.dart';
import 'package:we_match/model/member/member_join_request.dart';
import 'package:we_match/model/member/member_update_request.dart';


class RepoMember {
  Future<LoginResult> doLogin(LoginRequest loginRequest) async {
    const String baseUrl = '$apiUrl/login/app/user';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: loginRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return LoginResult.fromJson(response.data);
  }

  Future<LoginResult> setMember(MemberJoinRequest request) async {
    const String baseUrl = '$apiUrl/member/user/new';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return LoginResult.fromJson(response.data);
  }

  Future<CommonResult> putWithdraw() async {
    const String baseUrl = '$apiUrl/member/user/withdraw';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.delete(
      baseUrl,
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> putMemberUpdate(MemberUpdateRequest request) async {
    const String baseUrl = '$apiUrl/member/user/update';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    final response = await dio.put(
      baseUrl,
      data: request.toJson(),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      ),
    );

    return CommonResult.fromJson(response.data);
  }





}