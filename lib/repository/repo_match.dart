import 'package:dio/dio.dart';
import 'package:we_match/config/config_api.dart';
import 'package:we_match/functions/token_lib.dart';
import 'package:we_match/model/common_result.dart';
import 'package:we_match/model/match/match_apply_result.dart';
import 'package:we_match/model/match/match_detail_response.dart';
import 'package:we_match/model/match/match_request.dart';
import 'package:we_match/model/match/match_result.dart';

class RepoMatch {
  Future<CommonResult> setMatch(MatchRequest request) async {
    const String baseUrl = '$apiUrl/match/user/new';
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<MatchResult> getMyMatchList() async {
    const String baseUrl = '$apiUrl/match/user/list';
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    final response = await dio.get(
      baseUrl,
      options: Options(

        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return MatchResult.fromJson(response.data);
  }

  Future<MatchResult> getMatchList() async {
    const String baseUrl = '$apiUrl/match/common/list';
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    final response = await dio.get(
      baseUrl,
      options: Options(

        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return MatchResult.fromJson(response.data);
  }

  Future<MatchDetailResponse> getMatchDetail(int matchId) async {
    const String baseUrl = '$apiUrl/match/common/match-id/{matchId}';
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    final response = await dio.get(
      baseUrl.replaceAll('{matchId}', matchId.toString()),
      options: Options(

        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return MatchDetailResponse.fromJson(response.data);
  }



}